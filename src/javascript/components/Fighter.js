export default function Fighter(fighter, position) {
  const extendedFighter = {
    ...fighter,
    position,
    currentHealth: fighter.health,
    isBlocking: false,
    isReadyForCriticalAttack: true,
    setIsBlocking(value) {
      this.isBlocking = value;
    },
    setDamage(damage) {
      this.currentHealth -= damage;
    },
    async restartCriticalAttackTimer() {
      this.isReadyForCriticalAttack = false;
      await new Promise((resolve) => {
        setTimeout(resolve, 10000);
      });
      this.isReadyForCriticalAttack = true;
    },
  };
  return extendedFighter;
}

// TEST DID NOT PASS WHEN USING FIGHTER CLASS
// export default class Fighter {
//   constructor(obj, position) {
//     this.name = obj.name;
//     this.health = obj.health;
//     this.currentHealth = obj.health;
//     this.attack = obj.attack;
//     this.defense = obj.defense;
//     this.position = position;
//     this.isBlocking = false;
//     this.isReadyForCriticalAttack = true;
//   }
//   getName() {
//     return this.name;
//   }
//   getIsBlocking() {
//     return this.isBlocking;
//   }
//   getAttack() {
//     return this.attack;
//   }
//   getDefense() {
//     return this.defense;
//   }
//   getHealth() {
//     return this.health;
//   }
//   getCurrentHealth() {
//     return this.currentHealth;
//   }
//   getPosition() {
//     return this.position;
//   }
//   getIsReadyForCriticalAttack() {
//     return this.isReadyForCriticalAttack;
//   }
//   setIsReadyForCriticalAttack(value) {
//     this.isReadyForCriticalAttack = value;
//   }
//   setIsBlocking(value) {
//     this.isBlocking = value;
//   }
//   setDamage(damage) {
//     this.currentHealth -= damage;
//   }
//   async restartCriticalAttackTimer() {
//     this.isReadyForCriticalAttack=false;
//     await new Promise((resolve) => {
//       setTimeout(resolve, 10000);
//     });
//     this.isReadyForCriticalAttack=true;
//   }
// }