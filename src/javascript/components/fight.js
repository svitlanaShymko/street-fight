import { controls, GAME_CONTROLS, POSITION } from '../../constants/controls';
import { updateHealthIndicator } from './arena';
import Fighter from './Fighter';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterExtended = new Fighter(firstFighter, POSITION.LEFT);
    const secondFighterExtended = new Fighter(secondFighter, POSITION.RIGHT);

    const pressedKeys = new Map();

    document.addEventListener('keydown', (e) => {
      if (e.repeat || !GAME_CONTROLS.some((key) => key === e.code)) return;

      pressedKeys.set(e.code, true);

      processGameControlsPressed(firstFighterExtended, secondFighterExtended, pressedKeys, e.code);

      if (firstFighterExtended.currentHealth <= 0) {
        resolve(secondFighter);
      } else if (secondFighterExtended.currentHealth <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (e) => {
      if (e.code === controls.PlayerOneBlock) {
        firstFighterExtended.setIsBlocking(false);
      }
      if (e.code === controls.PlayerTwoBlock) {
        secondFighterExtended.setIsBlocking(false);
      }
      pressedKeys.delete(e.code);
    });
  });
}

function processGameControlsPressed(firstFighter, secondFighter, pressedKeys, key) {
  if (key === controls.PlayerOneAttack) {
    processAttack(firstFighter, secondFighter);
    return;
  }
  if (key === controls.PlayerTwoAttack) {
    processAttack(secondFighter, firstFighter);
    return;
  }

  if (controls.PlayerOneCriticalHitCombination.every((code) => pressedKeys.has(code))) {
    processCriticalAttack(firstFighter, secondFighter);
    return;
  }
  if (controls.PlayerTwoCriticalHitCombination.every((code) => pressedKeys.has(code))) {
    processCriticalAttack(secondFighter, firstFighter);
    return;
  }

  if (key === controls.PlayerOneBlock) {
    firstFighter.setIsBlocking(true);
  }
  if (key === controls.PlayerTwoBlock) {
    secondFighter.setIsBlocking(true);
  }
}

function processAttack(attacker, defender) {
  if (attacker.isBlocking) {
    return;
  }

  if (!defender.isBlocking) {
    const damage = getDamage(attacker, defender);
    if (damage) {
      defender.setDamage(damage);
      updateHealthIndicator(defender.currentHealth, defender.health, defender.position);
    }
  }
}

function processCriticalAttack(attacker, defender) {
  if (attacker.isBlocking) {
    return;
  }

  if (attacker.isReadyForCriticalAttack) {
    attacker.restartCriticalAttackTimer();
    const damage = getHitPower(attacker) * 2;
    defender.setDamage(damage);
    updateHealthIndicator(defender.currentHealth, defender.health, defender.position);
  }
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const randomNumber = Math.random() + 1;
  const power = fighter.attack * randomNumber;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const randomNumber = Math.random() + 1;
  const power = fighter.defense * randomNumber;
  return power;
}
