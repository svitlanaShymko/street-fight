import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-details',
    });
    fighterDetails.innerHTML = `
    <div>
    <h3>${fighter.name}</h3>
      <p>Attack: ${fighter.attack}</p>
     <p>Defense: ${fighter.defense}</p>
     <p>Health: ${fighter.health}</p>
      </div>
    `;
    fighterElement.append(fighterImage, fighterDetails);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
