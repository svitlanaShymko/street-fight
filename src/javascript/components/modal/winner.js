import { showModal } from './modal';
import { createFighterImage } from '../fightersView';

export function showWinnerModal(fighter) {
  const modalParameters = {
    title: `WINNER: ${fighter.name.toUpperCase()}`,
    bodyElement: createFighterImage(fighter),
    onClose: () => location.reload(),
  };

  showModal(modalParameters);
}
